<?php
namespace Credpago\Tuxedo;

abstract class Mask{
    const PHONE = '(##)#####-####';
    const CPF = '###.###.###-##';
    const CNPJ = '##.###.###/####-##';
    const CEP = '##.###-###';
    const MAC = '##:##:##:##:##:##';
}
