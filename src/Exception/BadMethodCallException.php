<?php
namespace Credpago\Tuxedo\Exception;

class BadMethodCallException extends \BadMethodCallException implements ExceptionInterface
{
}
