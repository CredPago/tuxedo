<?php
namespace Credpago\Tuxedo\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}
