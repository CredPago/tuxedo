<?php
namespace Credpago\Tuxedo\Exception;

class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}
