<?php
namespace Credpago\Tuxedo;

use Credpago\Tuxedo\Exception\RuntimeException;
use Credpago\Tuxedo\Mask;

/**
 * Tuxedo Utils: Generic utilities and powers to PHP projects
 * Some parts of this code are copied from https://github.com/jansenfelipe/faker-br
 *
 * @author Felipe Esteves <felipe.esteves@credpago.com>
 */
class Utils
{

    /**
     * Check CPF
     * 
     * @param  string $cpf
     * @return boolean
     */
    public static function isInvalidCpf($cpf)
    {
        $invalid = false;
        
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf);

        if (strlen($cpf) != 11) {

            $invalid = true;

        } else if (preg_match('/(\d)\1{10}/', $cpf)) {

            $invalid = true;

        } else {

            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf[$c] * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf[$c] != $d) {

                    $invalid = true;
                
                }
            }
        }

        return $invalid;
    }

    /**
     * Check CPF
     * 
     * @param  string $cpf
     * @return boolean
     */
    public static function isValidCpf($cpf)
    {
        return !self::isInvalidCpf($cpf);
    }
    
    /**
     * Check CEP
     * 
     * @param  string $cep
     * @return boolean
     */
    public static function isValidCep($cep)
    {
        return preg_match('/^[0-9]{5,5}([- ]?[0-9]{3,3})?$/', $cep);
    }

    /**
     * Check CPF
     * 
     * @param  string $cpf
     * @return boolean
     */
    public static function cpfInvalido($cpf)
    {
        return self::isInvalidCpf($cpf);
    }

    /**
     * Check CPF
     * 
     * @param  string $cpf
     * @return boolean
     */
    public static function cpfValido($cpf)
    {
        return !self::isInvalidCpf($cpf);
    }

    /**
     * Remove accents from string
     *
     * @param  string $str
     * @return string
     */
    public static function unaccents($str)
    {
        return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/", "/(Ç)/", "/(ç)/", "/(')/", "/(´)/"), explode(" ", "a A e E i I o O u U n N C c  "), $str);
    }

    /**
     * Remove accents from string
     *
     * @param  string $str
     * @return string
     */
    public static function removeAcentos($str)
    {
        return self::unaccents($str);
    }

    /**
     * Remove accents from string
     *
     * @param  string $str
     * @return string
     */
    public static function tirarAcentos($str)
    {
        return self::unaccents($str);
    }

    
    /**
     * Check CNPJ
     *
     * @param  string $cnpj
     * @return boolean
     */
    public static function isInvalidCnpj($cnpj)
    {
        $invalid = false;

        $cnpj = preg_replace( '/[^0-9]/is', '', $cnpj);
        
        if (strlen($cnpj) != 14) {

            $invalid = true;
            
        } else if (preg_match('/(\d)\1{14}/', $cnpj)) {

            $invalid = true;

        } else {
        
            for ($t = 12; $t < 14; $t++) {
                $d = 0;
                $c = 0;
                for ($m = $t - 7; $m >= 2; $m--, $c++) {
                    $d += $cnpj[$c] * $m;
                }
                for ($m = 9; $m >= 2; $m--, $c++) {
                    $d += $cnpj[$c] * $m;
                }
                $d = ((10 * $d) % 11) % 10;    
                if ($cnpj[$c] != $d) {
                    $invalid = false;
                    break;
                }
            }
        }

        return $invalid;
    }

    /**
     * Check CNPJ
     *
     * @param  string $cnpj
     * @return boolean
     */
    public static function isValidCnpj($cnpj)
    {
        return !self::isInvalidCnpj($cnpj);
    }

    /**
     * Check CNPJ
     *
     * @param  string $cnpj
     * @return boolean
     */
    public static function cnpjInvalido($cnpj)
    {
        return self::isInvalidCnpj($cnpj);
    }

    /**
     * Check CNPJ
     *
     * @param  string $cnpj
     * @return boolean
     */
    public static function cnpjValido($cnpj)
    {
        return !self::isInvalidCnpj($cnpj);
    }

    /**
     * Make a fake valid random CNPJ
     * 
     * @param boolean $real
     * @return string
     */
    public static function makeFakeCnpj($real = true) {
        $cnpj = rand(10000000, 99999999) . '0001';

        $aux = [5,4,3,2,9,8,7,6,5,4,3,2];
        $total = 0;

        foreach(str_split($cnpj) as $key => $char){
            $total += $char * $aux[$key];
        }

        $d1 = 11 - ($total % 11);
        $cnpj .= ($d1 >= 10)?'0':$d1;

        $aux = [6,5,4,3,2,9,8,7,6,5,4,3,2];
        $total = 0;
        
        foreach(str_split($cnpj) as $key => $char){
            $total += $char * $aux[$key];
        }

        $d2= 11 - ($total % 11);
        $cnpj .= ($d2 >= 10)?'0':$d2;

        if($real)
            $cnpj .= ($d2 >= 10)?'0':$d2;
        else
            $cnpj .= ($d2 >= 9)?'1':$d2+1;

        return $cnpj;
    }

    /**
     * Make a fake valid random CPF
     * 
     * @param boolean $real
     * @param boolean $formatted
     * @return string
     */
    public static function makeFakeCpf($real = true, $formatted = false) {
        $cpf = rand(100000000, 999999999);

        $aux = [10,9,8,7,6,5,4,3,2];
        $total = 0;

        foreach(str_split($cpf) as $key => $char){
            $total += $char * $aux[$key];
        }
        $d1 = 11 - ($total % 11);
        $cpf .= ($d1 >= 10)?'0':$d1;

        $aux = [11,10,9,8,7,6,5,4,3,2];
        $total = 0;
        
        foreach(str_split($cpf) as $key => $char){
            $total += $char * $aux[$key];
        }

        $d2= 11 - ($total % 11);
        $cpf .= ($d2 >= 10)?'0':$d2;
        
        if($real)
            $cpf .= ($d2 >= 10)?'0':$d2;
        else
            $cpf .= ($d2 >= 9)?'1':$d2+1;

        if($formatted){
            $cpf = Utils::mask($cpf, Mask::CPF);
        }

        return $cpf;
    }

    /**
     * Modulo calculator
     * 
     * @param int $dividend
     * @param int $divisor
     * @return int
     */
    public static function mod($dividend, $divisor) {
        return round($dividend - (floor($dividend / $divisor) * $divisor));
    }

    /**
     * Fix UTF characteres
     * 
     * @param string $str
     * @return string
     */
    public static function fixUtf($str)
    {
        $sample       = array('Ã“','Ã³','Ã','Ã¡','Ãƒ','Ã£','Ã‡','Ã§','Ã','Ã­','Ã','Ãª','Ã‰','Ã©','Ã','Ãµ','Ã','Ã¢','Ã','Ã','à´','Ã´','Ãƒ','â€“','ÃŠ','Ãº','Ãš','Ã‚','ã;','c;a;','Âª');
        $change       = array('Ó','ó','Á','á','Ã','ã','Ç','ç','Í','í','Ê','ê','É','é','Õ','õ','Â','â','À','Ô','ô','ô','Ã','-','Ê','ú','Ú','Â','ã','çã','ª');
        $specialchars = array('&Aacute;','&aacute;','&Acirc;','&acirc;','&Atilde;','&atilde','&Eacute;','&eacute;','&Ecirc;','&ecirc;',
                              '&Iacute;','&iacute;','&Icirc;','&icirc;','&Oacute;','&oacute;','&Ocirc;','&ocirc;','&Otilde;','&otilde',
                              '&Uacute;','&uacute;','&Ucirc;','&ucirc;','&Ccedil;', '&ccedil');
        $specialsubs  = array('Á', 'á','Â', 'â', 'Ã', 'ã', 'É', 'é', 'Ê', 'ê', 'Í', 'í', 'Î', 'î','Ó','ó','Ô','ô','Õ','õ','Ú','ú','Û','û','Ç','ç');
        return str_replace(array_merge($sample, $specialchars), array_merge($change, $specialsubs), $str);
    }

    /**
     * Fix UTF characteres
     * 
     * @param string $str
     * @return string
     */
    public static function refazUtf($str)
    {
        return self::fixUtf($str);
    }

    /**
     * Fix status imobiliaria characteres
     * 
     * @param string $str
     * @return string
     */
    public static function cleanImobStatus($str)
    {
        $status = self::fixUtf($str);
        return str_replace(' ', '_', Utils::removeAcentos(strtolower($status)));
    }

    /**
     * Refaz status imobiliaria characteres
     * 
     * @param string $str
     * @return string
     */
    public static function refazImobStatus($str)
    {
        return str_replace('_', ' ', ucwords($str));
    }

    /**
     * Remove special characters
     * 
     * @param string $str
     * @return string
     */
    public static function stringCleaner($str)
    {
        $str = strip_tags(trim($str));
        return preg_replace('/[^A-Za-z0-9]/', '', $str);
    }

    /**
     * Remove special characters
     * 
     * @param string $str
     * @return string
     */
    public static function limpador($str)
    {
        return self::stringCleaner($str);
    }

    /**
     * Add mask to string
     *
     * @param  string $str
     * @param  Mask $mask
     * @return string
     */
    public static function mask($str, $mask)
    {

        if (!empty($str)) {
            $str = self::unmask($str);
            $qtd = 0;
            for ($x = 0; $x < strlen($mask); $x++) {
                if ($mask[$x] == "#") {
                    $qtd++;
                }
            }
            if ($qtd > strlen($str)) {
                $str = str_pad($str, $qtd, "0", STR_PAD_LEFT);
            } elseif ($qtd < strlen($str)) {
                return $str;
            }
            if ($str <> '') {
                $string = str_replace(" ", "", $str);
                for ($i = 0; $i < strlen($string); $i++) {
                    $pos = strpos($mask, "#");
                    $mask[$pos] = $string[$i];
                }
                return $mask;
            }
        }
            
        return $str;
    }

    /**
     * Remove mask from string
     *
     * @param  string $str
     * @return string
     */
    public static function unmask($str)
    {
        return preg_replace('/[\-\|\(\)\/\.\: ]/', '', $str);
    }

    /**
     * Get real IP address
     *
     * @return string
     */
    public static function getRealIpAddr()
    {
        $ip = '';

        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)){

            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
    
        }else if (array_key_exists('REMOTE_ADDR', $_SERVER)) {
    
            $ip = $_SERVER["REMOTE_ADDR"];
    
        }else if (array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
    
            $ip = $_SERVER["HTTP_CLIENT_IP"];
    
        }
    
        return $ip;
    
    }

    /**
     * Get real IP address
     *
     * @return string
     */
    public static function buscaIp()
    {
        return self::getRealIpAddr();
    }

    /**
     * Convert money into human-readable words
     *
     * @param integer
     * @return string
     */
    public static function m2w($number, $money = true){
        return self::numberToWords($number, $money);
    }

    /**
     * Convert money into human-readable words
     *
     * @param integer
     * @return string
     */
    public static function moneyToWords($number, $money = true){
        return self::numberToWords($number, $money);
    }

    /**
     * Convert raw numbers into human-readable words
     *
     * @param integer
     * @return string
     */
    public static function n2w($number, $money = false){
        return self::numberToWords($number, $money);
    }

    /**
     * Convert raw numbers into human-readable words
     *
     * @param integer
     * @return string
     */
    public static function numberToWords($number, $money = false)
    {
        $hyphen = '-';
        $conjunction = ' e ';
        $separator = ' ';
        $negative = 'menos ';
        $decimal = ' e ';
        $dictionary = array(
            0 => 'zero',
            '00' => 'zero',
            1 => 'um',
            '01' => 'um',
            2 => 'dois',
            '02' => 'dois',
            3 => 'três',
            '03' => 'três',
            4 => 'quatro',
            '04' => 'quatro',
            5 => 'cinco',
            '05' => 'cinco',
            6 => 'seis',
            '06' => 'seis',
            7 => 'sete',
            '07' => 'sete',
            8 => 'oito',
            '08' => 'oito',
            9 => 'nove',
            '09' => 'nove',
            10 => 'dez',
            11 => 'onze',
            12 => 'doze',
            13 => 'treze',
            14 => 'quatorze',
            15 => 'quinze',
            16 => 'dezesseis',
            17 => 'dezessete',
            18 => 'dezoito',
            19 => 'dezenove',
            20 => 'vinte',
            30 => 'trinta',
            40 => 'quarenta',
            50 => 'cinquenta',
            60 => 'sessenta',
            70 => 'setenta',
            80 => 'oitenta',
            90 => 'noventa',
            100 => 'cento',
            200 => 'duzentos',
            300 => 'trezentos',
            400 => 'quatrocentos',
            500 => 'quinhentos',
            600 => 'seiscentos',
            700 => 'setecentos',
            800 => 'oitocentos',
            900 => 'novecentos',
            1000 => 'mil',
            1000000 => array('milhão', 'milhões'),
            1000000000 => array('bilhão', 'bilhões'),
            1000000000000 => array('trilhão', 'trilhões'),
            1000000000000000 => array('quatrilhão', 'quatrilhões'),
            1000000000000000000 => array('quinquilhão', 'quinquilhões')
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int)$number < 0) || (int)$number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words só aceita números entre ' . PHP_INT_MAX . ' à ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . self::numberToWords(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int)($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $conjunction . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = floor($number / 100) * 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds];
                if ($remainder) {
                    $string .= $conjunction . self::numberToWords($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int)($number / $baseUnit);
                $remainder = $number % $baseUnit;
                if ($baseUnit == 1000) {
                    $string = self::numberToWords($numBaseUnits) . ' ' . $dictionary[1000];
                } elseif ($numBaseUnits == 1) {
                    $string = self::numberToWords($numBaseUnits) . ' ' . $dictionary[$baseUnit][0];
                } else {
                    $string = self::numberToWords($numBaseUnits) . ' ' . $dictionary[$baseUnit][1];
                }
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= self::numberToWords($remainder);
                }
                break;
        }
        if ($money == true) {
            $string = $string . ' reais';
        } else {
            $string = $string;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = '';//array();
            //foreach (str_split((string)$fraction) as $number) {
            //    $words[] = $dictionary[$number];
            //}
            switch (true) {
                case $fraction < 21:
                    $words = $dictionary[$fraction];
                    break;
                case $fraction < 100:
                    $tens = ((int)($fraction / 10)) * 10;
                    $units = $fraction % 10;
                    $words = $dictionary[$tens];
                    if ($units) {
                        $words .= $conjunction . $dictionary[$units];
                    }
                    break;
            }
            if ($money == true) {
                $string .= $words . ' centavos';
            } else {
                $string .= $words;
            }
        }

        return $string;
    }

    /**
     * Date raw to words
     *
     * @param integer
     * @return string
     */
    public static function dateToWords($d=null){
        $day_of_week = ['Domingo','Segunda-feira', 'Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado'];
        $month = ['', 'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'];
        if($d==null) $date = time();
        else $date = strtotime($d);
        $res['week'] = $day_of_week[date('w', $date)];
        $res['month'] = $month[date('n', $date)];
        return $res;
    }

    /**
     * Is a valid date (american format)
     *
     * @param date
     * @param format
     * @return boolean
     */    
    public static function validDate($date, $format = 'Y-m-d H:i:s')
    {
        if (strlen($date) <= 10) {
            $format = 'Y-m-d';
        }

        return self::isDate($date, $format);
    }

    /**
     * Is a valid date (Brazilian format)
     *
     * @param date
     * @param format
     * @return boolean
     */    
    public static function dataValida($date, $format = 'd/m/Y H:i:s')
    {
        if (strlen($date) <= 10) {
            $format = 'd/m/Y';
        }

        return self::isDate($date, $format);
    }

    /**
     * Is a valid date
     *
     * @param date
     * @param format
     * @return boolean
     */ 
    public static function isDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * Is a valid json
     *
     * @param string
     * @return boolean
     */ 
    public static function isJson($string) 
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
