<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Credpago\Tuxedo\Utils;

final class DateTest extends TestCase
{
    public function testValidDateBrazilian()
    {
        $this->assertTrue(Utils::dataValida('11/11/2019'));
    }

    public function testValidDateBrazilianWithHour()
    {
        $this->assertTrue(Utils::dataValida('11/11/2019 12:12:12'));
    }
    
    public function testValidDate()
    {
        $this->assertTrue(Utils::validDate('2019-11-11'));
    }

    public function testInvalidDate()
    {
        $this->assertFalse(Utils::validDate('2019-11-32'));
    }

    public function testValidDateWithHours()
    {
        $this->assertTrue(Utils::validDate('2019-11-11 12:12:12'));
    }

    public function testInalidDateWithHours()
    {
        $this->assertFalse(Utils::validDate('2019-11-31 33:12:12'));
    }

    public function testValidDateWithFormat()
    {
        $this->assertFalse(Utils::validDate('2019-11-31 33:12', 'Y-m-d H:i'));
    }

    public function testInvalidDateWithFormat()
    {
        $this->assertFalse(Utils::validDate('2019-11-31 33:90', 'Y-m-d H:i'));
    }

    public function testDateToWords1()
    {
        $res = Utils::dateToWords('2019-07-31');
        $this->assertEquals(['week'=>'Quarta-feira','month'=>'Julho'], $res);
    }
}