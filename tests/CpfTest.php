<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Credpago\Tuxedo\Utils;

final class CpfTest extends TestCase
{
    public function testCpfInvalidFake()
    {

        for ($i = 1; $i <= 9; $i++) {
            $cpf = str_repeat("$i", 11);
            $return = Utils::isInvalidCpf($cpf);
    
            $this->assertTrue($return);
        }
        
    }

    public function testCpfInvalidWithoutFormat()
    {

        $cpf = "50421253020";
        $return = Utils::isInvalidCpf($cpf);

        $this->assertTrue($return);
    }

    public function testCpfValidWithoutFormat()
    {

        $cpf = "50421253029";
        $return = Utils::isInvalidCpf($cpf);

        $this->assertFalse($return);
    }

    public function testCpfInvalidLessCharLimit()
    {

        $cpf = "5042125302";
        $return = Utils::isInvalidCpf($cpf);

        $this->assertTrue($return);
    }

    public function testCpfInvalidOverCharLimit()
    {

        $cpf = "504212530299";
        $return = Utils::isInvalidCpf($cpf);

        $this->assertTrue($return);
    }

    public function testCpfInvalidWithFormat()
    {

        $cpf = "504.212.530-20";
        $return = Utils::isInvalidCpf($cpf);

        $this->assertTrue($return);
    }

    public function testCpfValidWithFormat()
    {

        $cpf = "504.212.530-29";
        $return = Utils::isInvalidCpf($cpf);

        $this->assertFalse($return);
    }

    public function testMakeRandomCpf()
    {

        $cpf = Utils::makeFakeCpf();
        $return = Utils::isInvalidCpf($cpf);

        $this->assertFalse($return);
    }

}



