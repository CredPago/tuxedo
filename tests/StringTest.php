<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Credpago\Tuxedo\Utils;

final class StringTest extends TestCase
{
    public function testStringCleaner()
    {
        $string = Utils::stringCleaner("111.111.111-11");
        $this->assertEquals("11111111111", $string);
    }

    public function testNumberToString()
    {
        $string = Utils::n2w(1);
        $this->assertEquals("um", $string);
    }

    public function testNumberToString2()
    {
        $string = Utils::n2w(1000);
        $this->assertEquals("um mil", $string);
    }

    public function testNumberToString3()
    {
        $string = Utils::n2w(10133);
        $this->assertEquals("dez mil cento e trinta e três", $string);
    }

    public function testNumberToString4()
    {
        $string = Utils::n2w(1324424.42);
        $this->assertEquals("um milhão trezentos e vinte e quatro mil quatrocentos e vinte e quatro e quarenta e dois", $string);
    }

    public function testMoneyToString4()
    {
        $string = Utils::m2w(1324424.42, true);
        $this->assertEquals("um milhão trezentos e vinte e quatro mil quatrocentos e vinte e quatro reais e quarenta e dois centavos", $string);
    }
}



