## Table of contents

- [\Credpago\Tuxedo\Utils](#class-credpagotuxedoutils)

<hr />

### Class: \Credpago\Tuxedo\Utils

> Tuxedo Utils: Generic utilities and powers to PHP projects Some parts of this code are copied from https://github.com/jansenfelipe/faker-br

| Visibility | Function |
|:-----------|:---------|
| public static | <strong>buscaIp()</strong> : <em>string</em><br /><em>Get real IP address</em> |
| public static | <strong>cnpjInvalido(</strong><em>string</em> <strong>$cnpj</strong>)</strong> : <em>boolean</em><br /><em>Check CNPJ</em> |
| public static | <strong>cnpjValido(</strong><em>string</em> <strong>$cnpj</strong>)</strong> : <em>boolean</em><br /><em>Check CNPJ</em> |
| public static | <strong>cpfInvalido(</strong><em>string</em> <strong>$cpf</strong>)</strong> : <em>boolean</em><br /><em>Check CPF</em> |
| public static | <strong>cpfValido(</strong><em>string</em> <strong>$cpf</strong>)</strong> : <em>boolean</em><br /><em>Check CPF</em> |
| public static | <strong>dataValida(</strong><em>mixed</em> <strong>$date</strong>, <em>string</em> <strong>$format=`'d/m/Y H:i:s'`</strong>)</strong> : <em>boolean</em><br /><em>Is a valid date (Brazilian format)</em> |
| public static | <strong>dateToWords(</strong><em>mixed</em> <strong>$d=null</strong>)</strong> : <em>string</em><br /><em>Date raw to words</em> |
| public static | <strong>fixUtf(</strong><em>string</em> <strong>$str</strong>)</strong> : <em>string</em><br /><em>Fix UTF characteres</em> |
| public static | <strong>getRealIpAddr()</strong> : <em>string</em><br /><em>Get real IP address</em> |
| public static | <strong>isDate(</strong><em>mixed</em> <strong>$date</strong>, <em>string</em> <strong>$format=`'Y-m-d H:i:s'`</strong>)</strong> : <em>boolean</em><br /><em>Is a valid date</em> |
| public static | <strong>isInvalidCnpj(</strong><em>string</em> <strong>$cnpj</strong>)</strong> : <em>boolean</em><br /><em>Check CNPJ</em> |
| public static | <strong>isInvalidCpf(</strong><em>string</em> <strong>$cpf</strong>)</strong> : <em>boolean</em><br /><em>Check CPF</em> |
| public static | <strong>isValidCnpj(</strong><em>string</em> <strong>$cnpj</strong>)</strong> : <em>boolean</em><br /><em>Check CNPJ</em> |
| public static | <strong>isValidCpf(</strong><em>string</em> <strong>$cpf</strong>)</strong> : <em>boolean</em><br /><em>Check CPF</em> |
| public static | <strong>limpador(</strong><em>string</em> <strong>$str</strong>)</strong> : <em>string</em><br /><em>Remove special characters</em> |
| public static | <strong>m2w(</strong><em>mixed</em> <strong>$number</strong>, <em>bool</em> <strong>$money=true</strong>)</strong> : <em>string</em><br /><em>Convert money into human-readable words</em> |
| public static | <strong>makeFakeCnpj(</strong><em>bool/boolean</em> <strong>$real=true</strong>)</strong> : <em>string</em><br /><em>Make a fake valid random CNPJ</em> |
| public static | <strong>makeFakeCpf(</strong><em>bool/boolean</em> <strong>$real=true</strong>, <em>bool/boolean</em> <strong>$formatted=false</strong>)</strong> : <em>string</em><br /><em>Make a fake valid random CPF</em> |
| public static | <strong>mask(</strong><em>string</em> <strong>$str</strong>, <em>\Credpago\Tuxedo\Mask</em> <strong>$mask</strong>)</strong> : <em>string</em><br /><em>Add mask to string</em> |
| public static | <strong>mod(</strong><em>int</em> <strong>$dividend</strong>, <em>int</em> <strong>$divisor</strong>)</strong> : <em>int</em><br /><em>Modulo calculator</em> |
| public static | <strong>moneyToWords(</strong><em>mixed</em> <strong>$number</strong>, <em>bool</em> <strong>$money=true</strong>)</strong> : <em>string</em><br /><em>Convert money into human-readable words</em> |
| public static | <strong>n2w(</strong><em>mixed</em> <strong>$number</strong>, <em>bool</em> <strong>$money=false</strong>)</strong> : <em>string</em><br /><em>Convert raw numbers into human-readable words</em> |
| public static | <strong>numberToWords(</strong><em>mixed</em> <strong>$number</strong>, <em>bool</em> <strong>$money=false</strong>)</strong> : <em>string</em><br /><em>Convert raw numbers into human-readable words</em> |
| public static | <strong>refazUtf(</strong><em>string</em> <strong>$str</strong>)</strong> : <em>string</em><br /><em>Fix UTF characteres</em> |
| public static | <strong>removeAcentos(</strong><em>string</em> <strong>$str</strong>)</strong> : <em>string</em><br /><em>Remove accents from string</em> |
| public static | <strong>stringCleaner(</strong><em>string</em> <strong>$str</strong>)</strong> : <em>string</em><br /><em>Remove special characters</em> |
| public static | <strong>tirarAcentos(</strong><em>string</em> <strong>$str</strong>)</strong> : <em>string</em><br /><em>Remove accents from string</em> |
| public static | <strong>unaccents(</strong><em>string</em> <strong>$str</strong>)</strong> : <em>string</em><br /><em>Remove accents from string</em> |
| public static | <strong>unmask(</strong><em>string</em> <strong>$str</strong>)</strong> : <em>string</em><br /><em>Remove mask from string</em> |
| public static | <strong>validDate(</strong><em>mixed</em> <strong>$date</strong>, <em>string</em> <strong>$format=`'Y-m-d H:i:s'`</strong>)</strong> : <em>boolean</em><br /><em>Is a valid date (american format)</em> |

