# Tuxedo

Lib to provide utilities and powers in PHP projects at CredPago

### Install

```
$ composer config repositories.credpago vcs https://gitlab.com/credpago/tuxedo
$ composer require credpago/tuxedo
```

### Usage

```
use Credpago/Tuxedo/Utils;
...
echo Utils::makeFakeCnpj();
```

### Run tests using Docker

```
$ docker run -u $UID -it -v $(pwd):/var/www/html credpago/php-apache:7-dev composer i
$ docker run -u $UID -it -v $(pwd):/var/www/html credpago/php-apache:7-dev phpunit
```

### More information

[Show me utilities and powers](./REFERENCE.md)

### To do

- [ ] More utilities
- [ ] More powers
- [ ] More tests